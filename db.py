import sqlite3
from sqlite3 import Error

UPDATE = "UPDATE library_activities SET checked_out_at = '{}' WHERE  library_activity_id = {}"

USER_INSERT_SQL = "INSERT INTO users(name) VALUES(?)"
BOOK_INSERT_SQL = "INSERT INTO books(title,author_name,isbn_num,genre,description) VALUES(?,?,?,?,?);"
LIBRARIES_INSERT_SQL = "INSERT INTO libraries(name,city,state,postal_code) VALUES(?,?,?,?);"
LIBRARY_BOOKS_INSERT_SQL = "INSERT INTO library_books(library_id,book_id,last_library_activity_id) VALUES(?,?,?);"

LIBRARY_ACTIVITIES_SQL = "INSERT INTO library_activities(user_id,library_book_id,activity_type," \
                         "checked_in_at) VALUES(?,?,?,?); "


def create_connection():
    conn = None
    try:
        conn = sqlite3.connect('library.db')
    except Error as e:
        print(e)
    return conn


class Table:
    @classmethod
    def insert(cls, response, sql):
        connection = create_connection()
        curses = connection.cursor()
        print(response)
        curses.execute(sql, response)
        connection.commit()
        connection.close()
        return {"id": curses.lastrowid, "message": "successfully created"}

    @classmethod
    def update(cls, response):
        connection = create_connection()
        curses = connection.cursor()
        print(response)
        curses.execute(response)
        connection.commit()
        connection.close()
        return {"message": "successfully Updated"}

    @classmethod
    def get(cls, response):
        connection = create_connection()
        curses = connection.cursor()
        print(response)
        data=curses.execute('SELECT * FROM library_activities')


        connection.commit()
        connection.close()
        return {}
