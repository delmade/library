import sqlite3


class LibraryDB:
    def __init__(self):
        self.connection = sqlite3.connect('library.db')
        self.cursor = self.connection.cursor()

    def create_tables(self):
        self.cursor.execute("CREATE TABLE IF NOT EXISTS libraries (library_id INTEGER PRIMARY KEY, name varchar(255) "
                            "NOT NULL, city varchar(30) NOT NULL,state varchar(30) NOT NULL,postal_code varchar(6) "
                            "NOT NULL);")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS books (book_id INTEGER PRIMARY KEY, title varchar(255) "
                            "NOT NULL, author_name varchar(50) NOT NULL,isbn_num varchar(30) NOT NULL,genre varchar("
                            "50) NOT NULL,description varchar(255) NOT NULL);")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS library_books (library_book_id INTEGER PRIMARY KEY, library_id "
                            "INTEGER ,last_library_activity_id INTEGER ,book_id INTEGER );")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS users (user_id INTEGER PRIMARY KEY, name varchar(255) );")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS library_activities (library_activity_id INTEGER PRIMARY KEY, "
                            "activity_type varchar(50),user_id INTEGER ,library_book_id INTEGER,checked_out_at "
                            "varchar(50),checked_in_at varchar(50)"
                            "  );")
        self.close_connection()

    def close_connection(self):
        self.connection.close()
        del self


LibraryDB().create_tables()

