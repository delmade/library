import datetime

from flask import Flask, request

import db
import validate_request
from db import USER_INSERT_SQL, BOOK_INSERT_SQL, LIBRARIES_INSERT_SQL, LIBRARY_BOOKS_INSERT_SQL, LIBRARY_ACTIVITIES_SQL, \
    UPDATE
from validate_request import USER, Book, LIBRARIES, LIBRARY_BOOKS, LIBRARY_ACTIVITIES

app = Flask(__name__)


@app.route('/create_user', methods=['POST'])
def create_user():
    validate, response = validate_request.check_request(USER)
    if validate:
        response = db.Table.insert(response, USER_INSERT_SQL)
    print(response)
    return response


@app.route('/add_book', methods=['POST'])
def add_book():
    validate, response = validate_request.check_request(Book)
    if validate:
        response = db.Table.insert(response, BOOK_INSERT_SQL)
    print(response)
    return response


@app.route('/add_libraries', methods=['POST'])
def add_libraries():
    validate, response = validate_request.check_request(LIBRARIES)
    if validate:
        response = db.Table.insert(response, LIBRARIES_INSERT_SQL)
    print(response)
    return response


@app.route('/add_library_books', methods=['POST'])
def add_library_books():
    validate, response = validate_request.check_request(LIBRARY_BOOKS)
    if validate:
        response = db.Table.insert(response, LIBRARY_BOOKS_INSERT_SQL)
    print(response)
    return response



@app.route('/checked_in_at', methods=['POST'])
def checked_in_at():
    x=datetime.datetime.now()
    request.json['checked_in_at']=x.strftime("%d-%m-%Y %H:%M")
    validate, response = validate_request.check_request(LIBRARY_ACTIVITIES)
    if validate:
        response = db.Table.insert(response, LIBRARY_ACTIVITIES_SQL)
    print(response)
    return response



@app.route('/checked_out_at', methods=['PUT'])
def checked_out_at():
    x=datetime.datetime.now()
    checked_out_at=x.strftime("%d-%m-%Y %H:%M")

    if 'library_activity_id' in request.json:
        response = db.Table.update(UPDATE.format(checked_out_at,request.json['library_activity_id']))
    print(response)
    return response



@app.route('/checked_out_at_list', methods=['GET'])
def checked_out_at_list():
    response = db.Table.get('out')
    print(response)


    return response

@app.route('/checked_in_at_list', methods=['GET'])
def checked_in_at_list():
    response = db.Table.get('in')
    return response


if __name__ == '__main__':
    app.run()
