from flask import request

USER = [["name", str]]
Book = [["title", str], ["author_name", str], ["isbn_num", str], ["genre", str], ["description", str]]
LIBRARIES = [['name', str], ['city', str], ['state', str], ['postal_code', str]]
LIBRARY_BOOKS = [['library_id', int], ['book_id', int], ['last_library_activity_id', int]]
LIBRARY_ACTIVITIES = [['user_id', int], ['library_book_id', int], ['activity_type', str], ['checked_in_at', str]]


def check_request(keys):
    payload = []
    for x in keys:
        if x[0] in request.json:
            try:
                payload.append(x[1](request.json[x[0]]))

            except:
                return False, {"message": "please check data type  " + x[0]}
        else:
            return False, {"message": "please fill user " + x[0]}
    else:
        return True, tuple(payload)
